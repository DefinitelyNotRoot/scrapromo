import requests
from lxml import html

pageContent=requests.get('https://www.x-kom.pl/')
# print(pageContent.content)
tree = html.fromstring(pageContent.content)
promo = tree.xpath('//*[@class="product-name"]/text()')
oldprice = tree.xpath('//*[@class="old-price"]/text()')
newprice = tree.xpath('//*[@class="new-price"]/text()')
print("x-kom.pl",promo,oldprice,newprice, "https://www.x-kom.pl/goracy_strzal/")


pageContent=requests.get('https://www.morele.net/')
tree = html.fromstring(pageContent.content)
promo = tree.xpath('//*[@class="promo-box-name"]//a/text()')
oldprice = tree.xpath('//*[@class="promo-box-old-price"]/text()')
newprice = tree.xpath('//*[@class="promo-box-new-price"]/text()')
url = tree.xpath('//*[@class="promo-box-name"]//a/@href')
print("morele.net",promo,oldprice,newprice, url[0])



pageContent=requests.get('https://helion.pl/promocje/#anchor_1')
tree = html.fromstring(pageContent.content)
promo = tree.xpath('//*[@class="book-title-in-promotion"]//a/text()')
newprice = tree.xpath('//*[@class="book-price"]//ins/text()')
oldprice = tree.xpath('//*[@class="book-price"]//del/text()')
print("Helion.pl",promo, "book:", [oldprice[0]],[newprice[0]] ,"ebook:",[oldprice[1]],[newprice[1]], "https://helion.pl/promocje/#anchor_1")

headers = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',}
pageContent=requests.get('https://proline.pl/',headers=headers)
tree = html.fromstring(pageContent.content)
promo = tree.xpath('//*[@class="center"]//a/text()')
oldprice = tree.xpath('//*[@class="cena_old"]/b/text()')
newprice = tree.xpath('//*[@class="cena_new"]/b/text()')
url = tree.xpath('//*[@class="center"]//a/@href')
print("Proline.pl/",promo,newprice, oldprice, "https://proline.pl" + url[0])
